#Histos
from CosmicRay import CosmicRay 
import matplotlib.pyplot as plt
import Range as r
from math import cos,log10,log,exp
from numpy import logspace
from scipy.integrate import dblquad

def getAllThetas(CRlist):
    return [CR.th*180/3.14 for CR in CRlist]

def getAllCosThetas(CRlist):
    return [CR.costh for CR in CRlist]

def getAllEi(CRlist):
    return [CR.Einit for CR in CRlist]

def getAllEf(CRlist):
    return [CR.Efinal for CR in CRlist]

def gen_data2(costh,scale=1): #Replicates DayaBay Fig3
    x,y=[],[]
    for i in range(1,7000,scale):
            x+=[i]
            y+=[(i**2.7)*funcdIdE(costh,Emu=i)]
    return x,y

def getDBvsCosTh(Emu,N=1000):
    x=[(i/N)*90 for i in range(0,N)]
    y=[(Emu**2.7)*r.dIdE(cos(j*3.14/180),Emu) for j in x]
    return x,y

def getminE(costh): #gfun(x)
    fdepth=85
    return r.muEnergy(fdepth/costh)

def dIdE(Emu,costh): #func(y,x)
    #From DayaBay paper, eqn2
    #Takes cos(angle of incidence), a muon energy, and returns Flux(?)
    costhetaS=r.getCosThetaCorrected(costh)

    B1=(1.1*Emu*costhetaS)/115
    B2=(1.1*Emu*costhetaS)/850
    B3=Emu*(costhetaS**1.29)
    
    A1=Emu*(1+(3.64/B3))
    A2=(1/(1+B1))+(0.054/(1+B2))

    final=0.14*(A1**-2.7)*A2
    return final

#minE=r.muEnergy(self.faserdepth/costh)

a1,a2=dblquad(dIdE,0,1,getminE,100000)
print(a1,a2)


##xlist=[]
##x=0
##attempts=0
##while len(xlist)<1000:
##    attempts+=1
##    x=CosmicRay()
##    #x.faserdepth=6000
##    x.fill()
##    if x.valid:
##        #print(x)
##        xlist+=[x]
##
###Histograms
##print('attempts:',attempts)
####thlist=getAllCosThetas(xlist)
####plt.xlabel('Cos(th)')
####thlist=getAllEi(xlist)
####plt.xlabel('Ei(GeV)')
##thlist=getAllEf(xlist)
##plt.xlabel('Ef(GeV)')
##binss=logspace(0,3,50)
##plt.hist(thlist, bins=binss)
##plt.ylabel('N')
##
####for E in [100,300,1000]:
####    X,Y=getDBvsCosTh(E)
####    plt.plot(X,Y,label=E)
####
####plt.yscale("log")
####plt.legend()
##plt.xscale("log")

plt.show()
