#muon class
from random import random
from math import cos,sin,acos
import Range as r
import numpy as np

class CosmicRay:
    def __init__(self,ptype='muon'):
        self.ptype=ptype
        self.coords=(0,0,0)
        self.Einit=0 #GeV
        self.Efinal=0 #GeV
        self.direction=(0,0,0) #
        self.costh=0
        self.th=0 #theta, radians
        self.faserdepth=85
        self.valid=True
        self.deepEnough=False
    def __str__(self):
        if self.valid:
            return f"Einit:{self.Einit}\nEfinal:{self.Efinal}\nTheta:{self.th}"
        else:
            return "Discarded."

    def fill(self,Edenom=0.09,disableKI=False):
        while not self.deepEnough:
            self.genCosTh()
            self.genEnergy(self.minEmu())
            self.deepEnough = self.checkDepth()
        val=(self.Einit**2.7)*r.dIdE(self.costh,self.Einit)/Edenom
        if r.keepit(val,random(),disable=disableKI):
            self.Efinal=r.muSlant(self.Einit,self.faserdepth,self.costh)

    def quickfill(self):
        self.genCosTh()

    def genCosTh(self,th_min=0,th_max=90):
        if th_min>th_max:
            print("Min angle greater than max angle!")
            return 0
        cosmin,cosmax=cos(th_min*3.14/180),cos(th_max*3.14/180)
        maxdiff=cosmax-cosmin
        self.costh=cosmin+(maxdiff*random())
        self.th=acos(self.costh)

    def genEnergy(self,Emin,Emax=100000,gamma=2.7): #Step 1 #N=number of energies
        self.Einit=(Emin**(1-gamma)+random()*(Emax**(1-gamma)-Emin**(1-gamma)))**(1/(1-gamma))

    def genCoords(self,R=5):
        p,psi=(R*random(),2*3.14*random())
        coords1=np.array(p*cos(psi),p*sin(psi),0)
        Rx = np.array((1,0,0),
                      (0,self.costh,-sin(self.th)),
                      (0,sin(self.th), self.costh))
        phi=2*3.14*random()
        Rz = np.array((cos(phi),-sin(phi),0),
                      (sin(phi), cos(phi),0),
                      (0,0,1))
        coords2 = np.dot(Rz,np.dot(Rx,coords1))

        translate_vector=np.array(R*sin(self.th)*cos(phi),R*sin(self.th)*sin(phi),R*self.costh)

        coords3=coords2+translate_vector
        self.coords=coords3

    def minEmu(self): #minimum energy to reach faser //at cos(theta)
        return r.muEnergy(self.faserdepth)

    def checkE(self):
        return self.Einit>r.muEnergy(self.faserdepth)

    def checkDepth(self):
        mulength=r.muRange(self.Einit)
        mudepth=self.depth()
        return mudepth>=self.faserdepth

    def depth(self): #vertical length component of particle path
        mulength=r.muRange(self.Einit)
        return mulength*self.costh

if __name__ == "__main__":
    xlist=[]
    while len(xlist)<10:
        x=CosmicRay()
        x.fill()
        if x.valid:
            print(x)
            xlist+=[x]
